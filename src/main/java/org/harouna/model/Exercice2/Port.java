package org.harouna.model.Exercice2;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.harouna.model.Exercice1.Bateau;

@Entity
@Table(name="t_Port")
public class Port {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) //G�n�re automatiquement d'id 
	@Column(name= "id")
	private long id;

	@Column(name="nom", length = 40)
	private String nom;

	@ManyToOne(cascade = CascadeType.PERSIST,
			fetch= FetchType.EAGER)
	private Pays pays;

	@OneToOne(mappedBy="port")
	private Bateau bateau;

	public Bateau getBateau() {
		return bateau;
	}

	public void setBateau(Bateau bateau) {
		this.bateau = bateau;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}

	@Override
	public String toString() {
		return "Port [id=" + id + ", nom=" + nom + "]";
	}

}
