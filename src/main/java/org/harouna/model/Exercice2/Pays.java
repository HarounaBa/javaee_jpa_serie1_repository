package org.harouna.model.Exercice2;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="t_Pays")
public class Pays {

	@Id @GeneratedValue(strategy = GenerationType.AUTO) //G�n�re automatiquement d'id 
	@Column(name= "id")
	private long id;

	@Column(name="nom", length= 40)
	private String nom;

	//Un pays peut avoir plusieurs ports
	@OneToMany(cascade= CascadeType.PERSIST,
			fetch = FetchType.EAGER,
			mappedBy= "pays")
	private List<Port> ports = new ArrayList();

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public List<Port> getPorts() {
		return ports;
	}

	public void setPorts(List<Port> ports) {
		this.ports = ports;
	}

	@Override
	public String toString() {
		return "Pays [id=" + id + ", nom=" + nom + "]";
	}



}
