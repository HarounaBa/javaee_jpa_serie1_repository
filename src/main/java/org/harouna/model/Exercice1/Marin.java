package org.harouna.model.Exercice1;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity 
@Table(name = "t_marin",
uniqueConstraints={ @UniqueConstraint(name="nom_prenom", columnNames={"nom", "prenom"})})
public class Marin {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private int id;

	@Column(name = "nom", length = 40) 
	private String nom;

	@Column(name = "prenom", length = 40)
	private String prenom;

	@Column(name = "date_de_naissance")
	@Temporal(TemporalType.DATE)
	private Date dateDeNaissance;

	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return "Marin [nom=" + nom + ", prenom=" + prenom + ", dateDeNaissance=" + dateDeNaissance + "]";
	}
}
