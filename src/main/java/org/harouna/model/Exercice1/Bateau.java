package org.harouna.model.Exercice1;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.eclipse.persistence.annotations.JoinFetch;
import org.eclipse.persistence.annotations.JoinFetchType;
import org.harouna.model.Exercice2.Port;
import org.harouna.model.Exercice2.Status;

@Entity
@Table(name="t_Bateau")
public class Bateau {

	@Id
	@GeneratedValue( strategy = GenerationType.AUTO )
	private int id;

	@Column(name= "nom", length =40)
	private String nom;

	@JoinColumn(name ="equipage")
	private List<Marin> equipage;

	@Enumerated(EnumType.STRING)
	@Column(name = "status", length = 15)
	private Status status;

	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER) 
	@JoinFetch(JoinFetchType.OUTER )
	private Port port;

	public List<Marin> getEquipage() {
		return equipage;
	}

	public void setEquipage(List<Marin> equipage) {
		this.equipage = equipage;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Port getPort() {
		return port;
	}

	public void setPort(Port port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "Bateau [id=" + id + ", nom=" + nom + ", equipage=" + equipage + ", status=" + status + "]";
	}
}
