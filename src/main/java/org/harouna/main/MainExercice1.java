package org.harouna.main;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Exercice1.Bateau;
import org.harouna.model.Exercice1.Marin;

public class MainExercice1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Calendar calendar = Calendar.getInstance();

		Marin marin1 = new Marin();

		marin1.setNom("Ba");
		marin1.setPrenom("Harouna");
		calendar.set(1993,1,6);
		marin1.setDateDeNaissance(calendar.getTime());

		Marin marin2 = new Marin();

		marin2.setNom("Ba");
		marin2.setPrenom("ElHadj");
		calendar.set(1980,5,6);
		marin2.setDateDeNaissance(calendar.getTime());

		/*****************************Partie Question 2***************************************/

		/**********Create**********/

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("marin");

		EntityManager em = emf.createEntityManager();

		//Generation code SQL, on prend les objets + �criture dans la base
		//On ouvre une transaction avant chaque cr�ation , 
		//modification ou suppression de la table
		//Ensuite on ferme la transaction

		em.getTransaction().begin();

		em.persist(marin1);
		em.persist(marin2);

		em.getTransaction().commit();

		/*************Retrieve***************/
		Marin marin = em.find(Marin.class, 1); //On part chercher la classe ayant pour id=1 et on le r�cup�re

		System.out.println("Marin pour PK 1 : " + marin);

		/**********Update *********/
		em.getTransaction().begin();

		marin = em.find(Marin.class, 1);
		calendar.set(1959, 1, 1);
		marin.setDateDeNaissance(calendar.getTime());

		em.getTransaction().commit();

		/*******Delete*********/
		em.getTransaction().begin();

		marin = em.find(Marin.class, 2);
		em.remove(marin);

		em.getTransaction().commit();	

		/*********************************Partie Question 4*******************************/

		Bateau bateau = new Bateau();
		bateau.setNom("Myboat");

		List<Marin> equipage = new ArrayList<Marin>();
		equipage.add(marin1);
		equipage.add(marin2);
		bateau.setEquipage(equipage);
		System.out.println(bateau);

		/**************Create*************/

		em.getTransaction().begin();
		em.persist(bateau);
		em.getTransaction().commit();

		/*************************Retrieve****************************/
		Bateau b = em.find(Bateau.class, 3); //On part chercher la classe ayant pour id=3 et on le r�cup�re
		System.out.println("Bateau pour PK 1 : " + b);

	}
}
