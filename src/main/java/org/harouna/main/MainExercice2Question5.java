package org.harouna.main;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Exercice1.Bateau;
import org.harouna.model.Exercice1.Marin;
import org.harouna.model.Exercice2.Pays;
import org.harouna.model.Exercice2.Port;
import org.harouna.model.Exercice2.Status;

public class MainExercice2Question5 {

	public static void main(String[] args) {

		Calendar calendar = Calendar.getInstance();

		Marin marin1 = new Marin();
		marin1.setNom("Ba");
		marin1.setPrenom("Harouna");
		calendar.set(1993,1,6);
		marin1.setDateDeNaissance(calendar.getTime());

		Marin marin2 = new Marin();
		marin2.setNom("Ba");
		marin2.setPrenom("ElHadj");
		calendar.set(1980,5,6);
		marin2.setDateDeNaissance(calendar.getTime());

		Bateau bateau = new Bateau();
		bateau.setNom("Myboat");

		bateau.setStatus(Status.PORT);

		List<Marin> equipage = new ArrayList<Marin>();
		equipage.add(marin1);
		equipage.add(marin2);
		bateau.setEquipage(equipage);

		Pays pays = new Pays();

		pays.setNom("France");

		Port port = new Port();
		port.setNom("Port de marseille");
		port.setPays(pays);
		pays.getPorts().add(port);

		bateau.setPort(port);
		port.setBateau(bateau);


		/***********Create*************/

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("marin");
		EntityManager em = emf.createEntityManager();

		em.getTransaction().begin();

		em.persist(marin1);
		em.persist(marin2);
		em.persist(bateau);
		em.persist(port);

		em.getTransaction().commit();

		System.out.println(bateau);
		System.out.println(port);
		System.out.println(pays);
	}
}
