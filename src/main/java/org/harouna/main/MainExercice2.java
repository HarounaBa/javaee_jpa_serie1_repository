package org.harouna.main;

import java.util.Calendar;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.harouna.model.Exercice2.Pays;
import org.harouna.model.Exercice2.Port;

public class MainExercice2 {

	public static void main(String[] args) {

		Pays pays = new Pays();
		pays.setNom("France");

		Port port1 = new Port();
		port1.setNom("Port de marseille");
		port1.setPays(pays);

		Port port2 = new Port();
		port2.setNom("Port de Paris");
		port2.setPays(pays);

		pays.getPorts().add(port1);
		pays.getPorts().add(port2);

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("marin");
		EntityManager em = emf.createEntityManager();

		/******************Create***************/
		em.getTransaction().begin();

		em.persist(pays);

		em.getTransaction().commit();

		System.out.println(pays);
		System.out.println(pays.getPorts());
	}
}
